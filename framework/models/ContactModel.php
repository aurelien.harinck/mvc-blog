<?php
/**
 * Created by PhpStorm.
 * User: aurelienharinck
 * Date: 07/10/2018
 * Time: 13:22
 */

class ContactModel {
    private static $_instance =  null;

    private $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    /**
     * Renvoie de l'instance et initialisation si nécessaire.
     */
    public static function getInstance ()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new ContactModel();
        }
        return self::$_instance;
    }

    /**
     * Insérer un nouveau contact
     */
    public function insert($params) {

        // GET LAST ID
        $statement = $this->db->getDb()->prepare("SELECT max(id) as id FROM contact");
        $statement->execute();

        $result = $statement->fetch();

        if ($result) {
           $id = $result['id'] + 1;
        }else {
            $id = 1;
        }

        $statement = $this->db->getDb()->prepare("INSERT INTO contact VALUES (:id, :nom, :phone, :text)");
        $statement->bindParam(':id', $id);
        $statement->bindParam(':nom', $params['name']);
        $statement->bindParam(':phone', $params['phone']);
        $statement->bindParam(':text', $params['text']);
        $result = $statement->execute();

        return $result;
    }
}
