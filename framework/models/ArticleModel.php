<?php

class ArticleModel {
    private static $_instance =  null;

    private $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    /**
     * Renvoi de l'instance et initialisation si nécessaire.
     */
    public static function getInstance ()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new ArticleModel();
        }

        return self::$_instance;
    }

    public function getAll() {
        $statement = $this->db->getDb()->prepare("SELECT id, title, text from article");
        $statement->execute();
        $result = $statement->fetchAll();
        if ($result) {
            return $result;
        }
        return null;
    }
}
