<?php
/**
 * Created by PhpStorm.
 * User: aurelienharinck
 * Date: 26/09/2018
 * Time: 10:05
 */

require __DIR__ . '/../../config.php';

class Database
{
    private static $_instance =  null;

    private $db;

    public function __construct()
    {
        try {
            $this->db = new \PDO('mysql:host='. database_host . ';dbname=' . database_name, database_username, database_password);
        } catch (\PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
        }
    }

    /**
     * Renvoi de l'instance et initialisation si nécessaire.
     */
    public static function getInstance ()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new Database();
        }

        return self::$_instance;
    }

    public function getDb()
    {
        return $this->db;
    }

}
