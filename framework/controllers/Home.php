<?php
/**
 * Created by PhpStorm.
 * User: aurelienharinck
 * Date: 26/09/2018
 * Time: 10:05
 */

require __DIR__ . "/../models/Database.php";
require __DIR__ . "/../models/ArticleModel.php";

class Home
{

    private $database;

    public function __construct()
    {
        $this->database = Database::getInstance();
        $this->index();
    }

    public function index()
    {
        $article = ArticleModel::getInstance();
        $articles = $article->getAll();

        ob_start();
        if ($articles) {
            extract($articles);
        }
        require(__DIR__ . '/../views/home.php');
        return ob_end_flush();
    }

    /* public function connexion($login, $password)
    {
        $statement = $this->database->getDb()->prepare('SELECT login from connexion where login = :login and password = :password');

        $result = $statement->execute(array(
            'login' => $login,
            'password' => $password
        ));


        if ($result) {
            echo "<h1> Bienvenue sur le site </h1>";
        } else {
            echo $this->vue(true);
        }
    } */
}
