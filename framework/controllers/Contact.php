<?php

require __DIR__ . '/../models/ContactModel.php';

class Contact {
    public function __construct()
    {
        $this->index();
    }

    public function index() {
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : null;
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : null;
        $text = isset($_REQUEST['text']) ? $_REQUEST['text'] : null;

        if (isset($name) && isset($phone) && isset($text)) {
            $contactModel = ContactModel::getInstance();
            $contactModel->insert([
                'name' => $name,
                'phone' => $phone,
                'text' => $text
            ]);
        }
        ob_start();
        require(__DIR__ . '/../views/contact.php');
        return ob_end_flush();
    }
}
